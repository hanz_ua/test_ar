package com.bvblogic.tickoweb.api.manager.core;

import com.bvblogic.tickoweb.api.core.BaseView.BaseView;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseDataManager<T, Y> {
    protected final T service;
    protected final BaseView<Y> view;
    protected CompositeDisposable compositeDisposable;

    public BaseDataManager(T service, BaseView<Y> view) {
        this.service = service;
        this.view = view;
        compositeDisposable = new CompositeDisposable();
    }
}
