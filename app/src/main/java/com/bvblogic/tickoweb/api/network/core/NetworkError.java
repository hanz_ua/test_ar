package com.bvblogic.tickoweb.api.network.core;

import android.util.Log;

import com.bvblogic.tickoweb.api.domain.ErrorModel;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

/**
 * Created by Misha Dichuk on 23.04.18.
 */
public class NetworkError extends Throwable {
    public static final String ERROR_MESSAGE = "Щось пішло не так! Попробуйте ще раз.";
    private static final String NETWORK_ERROR_MESSAGE = "Немає з'єднання з інтернетом!";
    private static final String NETWORK_ERROR_EMAIL_OR_PASS = "Неправильний введений електронний лист або пароль";

    public static final int NO_CONNECTION_INTERNET = 400;
    public static final int UNAUTHORIZED_DATA = 401;
    public static final int UNPROCESSABLE_ENTITY = 422;
    public static final int SOMERHING_WENT_WRONG = 600;
    private final Throwable error;
    private String errorTemp;


    public NetworkError(Throwable error) {
        this.error = error;
    }

    public boolean isAuthFailure() {
        return error instanceof HttpException &&
                ((HttpException) error).code() == HTTP_UNAUTHORIZED;
    }

    public boolean isResponseNull() {
        return error instanceof HttpException && ((HttpException) error).response() == null;
    }

    public int getCode() {
        return ((HttpException) this.error).response().code();
    }

    public Throwable getError() {
        return error;
    }

    @Override
    public String getMessage() {
        return error.getMessage();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetworkError that = (NetworkError) o;

        return error != null ? error.equals(that.error) : that.error == null;
    }

    @Override
    public int hashCode() {
        return error != null ? error.hashCode() : 0;
    }


    public ErrorModel getAppErrorModel() {
        Log.d("Error", error.getMessage());
        if (this.error instanceof IOException)
            return new ErrorModel(String.valueOf(NO_CONNECTION_INTERNET), NETWORK_ERROR_MESSAGE);
        if (!(this.error instanceof HttpException))
            return new ErrorModel(String.valueOf(SOMERHING_WENT_WRONG), ERROR_MESSAGE);
        retrofit2.Response<?> response = ((HttpException) this.error).response();
        if (response != null) {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                if (!jsonObject.isNull("message")) {
                    return new ErrorModel(String.valueOf(UNPROCESSABLE_ENTITY),
                            jsonObject.getString("message"));
                }
                JSONObject errors = jsonObject.getJSONObject("errors");
                Log.d("Error", errors.keys().next());
                return new ErrorModel(String.valueOf(UNPROCESSABLE_ENTITY),
                        errors.getJSONArray(errors.keys().next()).getString(0));
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            switch (response.code()) {
                case UNAUTHORIZED_DATA:
                    return new ErrorModel(String.valueOf(UNAUTHORIZED_DATA),
                            NETWORK_ERROR_EMAIL_OR_PASS);
            }
        }
        return new ErrorModel(String.valueOf(SOMERHING_WENT_WRONG), ERROR_MESSAGE);
    }
}

