package com.bvblogic.tickoweb.api.domain;


import com.bvblogic.tickoweb.api.domain.core.Model;

/**
 * Created by Misha Dichuk on 23.04.18.
 */
public class ErrorModel extends Model {

    private String title;
    private String description;

    public ErrorModel(String title, String description) {
        this.title = title;
        this.description = description;
    }


    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
