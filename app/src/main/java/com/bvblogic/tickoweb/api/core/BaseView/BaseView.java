package com.bvblogic.tickoweb.api.core.BaseView;


import com.bvblogic.tickoweb.api.domain.ErrorModel;

/**
 * Created by Misha Dichuk on 19.04.18.
 */
public interface BaseView<T> {

    void showWait();

    void hideWait();

    void onError(ErrorModel errorModel);

}
