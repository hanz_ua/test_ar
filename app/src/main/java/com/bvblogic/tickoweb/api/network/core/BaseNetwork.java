package com.bvblogic.tickoweb.api.network.core;


import com.bvblogic.tickoweb.api.domain.ErrorModel;

/**
 * Created by Misha Dichuk on 19.04.18.
 */
public class BaseNetwork<T> {

    protected final T networkService;

    public BaseNetwork(T networkService) {
        this.networkService = networkService;
    }

    public interface Callback<T> {
        void onSuccess(T t);

        void onError(ErrorModel errorModel);
    }
}
