package com.bvblogic.tickoweb.fragment;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.fragment.core.BaseFragment;
import com.bvblogic.tickoweb.mvp.core.ToolBarById;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_main)
public class MainFragment extends BaseFragment {

    @AfterViews
    public void initToolbar(){
        initToolBar(ToolBarById.SIMPLE);
    }

}
