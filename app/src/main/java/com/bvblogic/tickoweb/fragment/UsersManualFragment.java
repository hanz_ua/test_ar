package com.bvblogic.tickoweb.fragment;

import android.os.Bundle;
import android.widget.TextView;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.fragment.core.BaseFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.fragment_users_manual)
public class UsersManualFragment extends BaseFragment {

    @FragmentArg
    String text;

    @ViewById(R.id.tv_description)
    protected TextView tvDescription;

    @AfterViews
    public void initToolbar() {
        if (text != null) {
            tvDescription.setText(text);
        }

    }
}