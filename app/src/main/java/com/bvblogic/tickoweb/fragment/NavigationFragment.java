package com.bvblogic.tickoweb.fragment;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.fragment.core.BaseFragment;

import org.androidannotations.annotations.EFragment;

@EFragment(R.layout.fragment_navigation)
public class NavigationFragment extends BaseFragment {
}
