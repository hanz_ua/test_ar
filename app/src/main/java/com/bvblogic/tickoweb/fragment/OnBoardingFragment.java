package com.bvblogic.tickoweb.fragment;

import android.support.v4.view.ViewPager;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.adapter.ViewPagerAdapter;
import com.bvblogic.tickoweb.fragment.core.BaseFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.util.List;
import java.util.Objects;

@EFragment(R.layout.fragment_onboarding)
public class OnBoardingFragment extends BaseFragment implements ViewPager.OnPageChangeListener {

    private int page = 0;   //  to track page position

    @ViewsById({R.id.intro_indicator_0, R.id.intro_indicator_1, R.id.intro_indicator_2, R.id.intro_indicator_3})
    protected List<ImageView> indicators;

    @ViewById(R.id.btn_getstarted)
    protected Button btnGetStarted;

    @ViewById(R.id.pager)
    protected ViewPager mViewPager;

    @AfterViews
    public void initFragment() {
        setFullScreen();
        ViewPagerAdapter mAdapter = new ViewPagerAdapter(getContext(), Objects.requireNonNull(getActivity()).getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        updateIndicators(page);
        mViewPager.addOnPageChangeListener(this);
    }


    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        page = i;
        updateIndicators(page);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    private void updateIndicators(int position) {
        for (int i = 0; i < indicators.size(); i++) {
            indicators.get(i).setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }

    @Click(R.id.btn_getstarted)
    protected void onClickGetStarted() {
        Toast.makeText(getActivity(), "OnClickGetStarted", Toast.LENGTH_SHORT).show();
    }


}
