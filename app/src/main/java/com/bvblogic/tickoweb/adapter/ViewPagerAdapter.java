package com.bvblogic.tickoweb.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.fragment.UsersManualFragment_;

import org.androidannotations.annotations.EBean;


/**
 * Created by fir on 28.10.15.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private static final String TEXT = "text";

    private String[] mFragmentStrings;
    private Context mContext;

    public ViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
        mFragmentStrings = mContext.getResources().getStringArray(R.array.onboard_text);
    }

    @Override
    public Fragment getItem(int position) {
        return UsersManualFragment_.builder().arg(TEXT, mFragmentStrings[position]).build();
    }

    @Override
    public int getCount() {
        return mFragmentStrings.length;
    }
}
