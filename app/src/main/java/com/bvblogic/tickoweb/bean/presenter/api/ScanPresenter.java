package com.bvblogic.tickoweb.bean.presenter.api;

import com.bvblogic.tickoweb.bean.core.BasePresenter;
import com.bvblogic.tickoweb.bean.presenter.api.core.ScanBasePresenter;

import org.androidannotations.annotations.EBean;

@EBean
public class ScanPresenter extends ScanBasePresenter {

    public void sendScan(BasePresenter basePresenter) {
        setBasePresenter(basePresenter);
    }
}
