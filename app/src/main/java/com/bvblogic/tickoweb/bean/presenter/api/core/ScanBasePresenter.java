package com.bvblogic.tickoweb.bean.presenter.api.core;

import com.bvblogic.tickoweb.bean.core.BasePresenter;

import org.androidannotations.annotations.EBean;

@EBean
public class ScanBasePresenter {
    protected BasePresenter basePresenter;

    public void setBasePresenter(BasePresenter basePresenter) {
        this.basePresenter = basePresenter;
    }
}
