package com.bvblogic.tickoweb.bean.core.dialog;

import android.app.AlertDialog;
import android.support.v4.app.FragmentActivity;

import com.bvblogic.tickoweb.R;


/**
 * Created by Misha Dichuk on 23.04.18.
 */
public class AlertMessageErrorDialog {

    public static void showAlertDialog(FragmentActivity activity, String title, String description) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogLightStyle);
        builder.setMessage(description)
                .setPositiveButton(activity.getResources().getString(R.string.ok), (dialog, id) -> dialog.cancel());
        builder.show();
    }
}
