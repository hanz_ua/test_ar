package com.bvblogic.tickoweb.bean.core;


import com.bvblogic.tickoweb.activity.MainActivity;
import com.bvblogic.tickoweb.activity.core.BaseActivity;
import com.bvblogic.tickoweb.api.core.BaseView.BaseView;
import com.bvblogic.tickoweb.api.domain.ErrorModel;
import com.bvblogic.tickoweb.api.network.core.NetworkError;
import com.bvblogic.tickoweb.bean.core.dialog.AlertMessageErrorDialog;
import com.bvblogic.tickoweb.bean.preference.PreferencesBean;
import com.bvblogic.tickoweb.fragment.core.BaseFragment;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by hanz on 23.04.2018.
 */

@EBean
public abstract class BasePresenter implements BaseView {
    @RootContext
    protected MainActivity baseActivity;

    @Bean
    protected PreferencesBean preferencesDataManager;

    @Override
    public void showWait() {
        if (baseActivity != null) {
            baseActivity.showProgressBar();
        }
    }

    @Override
    public void hideWait() {
        if (baseActivity != null) {
            baseActivity.hideProgressBar();
        }
    }

    @Override
    public void onError(ErrorModel errorModel) {
        AlertMessageErrorDialog.showAlertDialog(baseActivity,
                errorModel.getTitle(), errorModel.getDescription());
    }
}
