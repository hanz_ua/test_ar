package com.bvblogic.tickoweb.bean.presenter;

import com.bvblogic.tickoweb.bean.core.BasePresenter;
import com.bvblogic.tickoweb.bean.presenter.api.ScanPresenter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

@EBean
public class MainPresenter extends BasePresenter {

    @Bean
    ScanPresenter scanPresenter;

    public void scan() {
        scanPresenter.sendScan(this);
    }


}
