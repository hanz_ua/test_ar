package com.bvblogic.tickoweb.bean.preference;


import com.bvblogic.tickoweb.bean.preference.core.Preference;

import org.androidannotations.annotations.EBean;

/**
 * Created by hanz on 10.05.2017.
 */

@EBean
public class PreferencesBean extends Preference {

    public void saveToken(String token) {
        savePreferences(TOKEN_KEY, "token " + token);
    }

    public String getToken() {
        return getPreferencesString(TOKEN_KEY);
    }

    public void removeToken() {
        remove(TOKEN_KEY);
    }

}
