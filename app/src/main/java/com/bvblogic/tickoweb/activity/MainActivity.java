package com.bvblogic.tickoweb.activity;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.activity.core.BaseActivity;
import com.bvblogic.tickoweb.mvp.core.FragmentById;
import com.bvblogic.tickoweb.mvp.core.FragmentData;
import com.bvblogic.tickoweb.mvp.manager.MainActivityManagerUI;
import com.bvblogic.tickoweb.mvp.manager.core.ManagerUI;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {

    @Override
    protected ManagerUI getManagerUIToInit() {
        return new MainActivityManagerUI(this);
    }

    @AfterViews
    public void init(){
        changeFragmentTo(new FragmentData(FragmentById.ONBOARDING_FRAGMENT));
    }
}
