package com.bvblogic.tickoweb.activity.core;

import android.app.Activity;
import android.view.WindowManager;

public class UtilFullScrean {

    protected static void setFullScreen(Activity activity) {
        if (activity != null)
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected static void cleanFullScreen(Activity activity) {
        if (activity != null)
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
