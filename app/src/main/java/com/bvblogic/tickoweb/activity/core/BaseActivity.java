package com.bvblogic.tickoweb.activity.core;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.fragment.core.BaseFragment;
import com.bvblogic.tickoweb.mvp.core.BaseFragmentData;
import com.bvblogic.tickoweb.mvp.core.FragmentFeedback;
import com.bvblogic.tickoweb.mvp.core.ToolBarById;
import com.bvblogic.tickoweb.mvp.manager.core.ManagerUI;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity
public abstract class BaseActivity extends AppCompatActivity implements FragmentFeedback {
    private ManagerUI managerUI;

    @ViewById(R.id.progress_bar)
    public View progressBar;

    @AfterInject
    protected void create() {
        managerUI = this.getManagerUIToInit();
    }

    @Override
    public void initToolBar(BaseFragment baseFragment, ToolBarById toolBarById, int... label) {
        this.managerUI.initToolbar(baseFragment, toolBarById, label);
    }

    protected abstract ManagerUI getManagerUIToInit();

    @Override
    public void changeFragmentTo(BaseFragmentData baseFragmentData) {
        this.managerUI.changeFragmentTo(baseFragmentData);
    }

    public void showProgressBar() {
        if (progressBar.getVisibility() != View.VISIBLE) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressBar() {
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

}
