package com.bvblogic.tickoweb.mvp.core;

import android.os.Bundle;

import java.util.List;

public abstract class BaseFragmentData {

    protected Object[] bundle;

    public BaseFragmentData setBundle(Object... bundle) {
        this.bundle = bundle;
        return this;
    }

    public Object[] getBundle() {
        return bundle;
    }

    public abstract FragmentById getFragmentById();

}
