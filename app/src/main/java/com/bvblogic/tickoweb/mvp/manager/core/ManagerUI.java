package com.bvblogic.tickoweb.mvp.manager.core;

import android.support.annotation.StringRes;

import com.bvblogic.tickoweb.fragment.core.BaseFragment;
import com.bvblogic.tickoweb.mvp.core.BaseFragmentData;
import com.bvblogic.tickoweb.mvp.core.ToolBarById;

public interface ManagerUI {
    public static int ADD_BACK_STACK = 1;
    public static int ADD = 2;
    public static int REPLACE = 3;
    public static int REMOVE_LAST_FRAGMENT_ADD_TO_BACK_STACK = 4;

    void changeFragmentTo(BaseFragmentData baseFragmentData);

    void initToolbar(BaseFragment baseFragment, ToolBarById toolBarById, @StringRes int... text);
}
