package com.bvblogic.tickoweb.mvp.core;


import android.support.annotation.StringRes;

import com.bvblogic.tickoweb.fragment.core.BaseFragment;

public interface FragmentFeedback {

    void changeFragmentTo(BaseFragmentData baseFragmentData);

    void initToolBar(BaseFragment baseFragment, ToolBarById toolBarById, @StringRes int... label);

}
