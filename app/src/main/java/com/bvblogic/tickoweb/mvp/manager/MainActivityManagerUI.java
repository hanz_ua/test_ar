package com.bvblogic.tickoweb.mvp.manager;

import android.support.v7.app.AppCompatActivity;

import com.bvblogic.tickoweb.R;
import com.bvblogic.tickoweb.fragment.MainFragment_;
import com.bvblogic.tickoweb.fragment.NavigationFragment_;
import com.bvblogic.tickoweb.fragment.OnBoardingFragment_;
import com.bvblogic.tickoweb.fragment.ScanFragment_;
import com.bvblogic.tickoweb.fragment.ScanGreenFragment_;
import com.bvblogic.tickoweb.fragment.ScanRedFragment_;
import com.bvblogic.tickoweb.fragment.ScanYellowFragment_;
import com.bvblogic.tickoweb.fragment.SettingsFragment_;
import com.bvblogic.tickoweb.mvp.core.BaseFragmentData;
import com.bvblogic.tickoweb.mvp.manager.core.BaseMainActivityManagerUI;
import com.bvblogic.tickoweb.mvp.manager.core.ManagerUI;

public class MainActivityManagerUI extends BaseMainActivityManagerUI {

    public MainActivityManagerUI(AppCompatActivity activity) {
        super(activity);
    }

    @Override
    protected int getIdFragmentsContainer() {
        return R.id.fragment_container;
    }

    @Override
    public void changeFragmentTo(BaseFragmentData baseFragmentData) {
        switch (baseFragmentData.getFragmentById()) {
            case MAIN_FRAGMENT: {
                addFragmentToContainer(MainFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }
            case NAVIGATION_FRAGMENT: {
                addFragmentToContainer(NavigationFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }
            case ONBOARDING_FRAGMENT: {
                addFragmentToContainer(OnBoardingFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }
            case SCAN_FRAGMENT: {
                addFragmentToContainer(ScanFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }
            case SCAN_GREEN_FRAGMENT: {
                addFragmentToContainer(ScanGreenFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }
            case SCAN_RED_FRAGMENT: {
                addFragmentToContainer(ScanRedFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }
            case SCAN_YELLOW_FRAGMENT: {
                addFragmentToContainer(ScanYellowFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }
            case SETTINGS_FRAGMENT: {
                addFragmentToContainer(SettingsFragment_.builder().build(), ManagerUI.ADD_BACK_STACK);
                break;
            }

        }
    }
}
